import React from 'react';
import styled from "styled-components"


const ContenedorFrase = styled.div`
  padding: 3rem;
  border-radius: 0.5rem;
  max-width: 800px;
  h1{
    font-family: Arial, Helvetica, sans-serif;
    text-align: center;
    position: relative;
    padding-left: 4rem;
  }
  p {
    font-family: Verdana, Geneva, Tahoma, sans-serif;
    text-align: right;
    font-weight: bold;
    color: azure;
    margin-top:2rem
  }
`;


const Frase = ({ sasa }) => {
  return (
    <ContenedorFrase>
      <h1>{sasa.quote} </h1>
      <p>-{sasa.author} </p>
    </ContenedorFrase>
  );
};

export default Frase;