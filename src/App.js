import React, { useState, useEffect } from "react";
import styled from "styled-components";
import "./App.css";
import Frase from "./Componentes/Frase";

const Contenedor = styled.div`
  display: flex;
  align-items: center;
  padding-top: 5.5rem;
  flex-direction: column;
`;

const Boton = styled.button`
  background: -webkit-linear-gradient(
    top left,
    #007d35 0%,
    #007d35 40%,
    #0f574e 100%
  );
  background-size: 300px;
  font-family: Arial, Helvetica, sans-serif;
  color: #fff;
  margin-top: 3rem;
  padding: 1rem 3rem;
  font-size: 2rem;
  border: 2px solid black;
  transition: background-size 0.8s ease;
  :hover {
    cursor: pointer;
    background-size: 400px;
  }
`;

function App() {
  // state de frase
  //hola
  const [frase, setFrase] = useState({});

  //carga frase
  useEffect(() => {
    consultarAPI();
  },
  []);

  const consultarAPI = async () => {

    const api = await fetch("https://breaking-bad-quotes.herokuapp.com/v1/quotes");
    //console.log(api)
    const res = await api.json();
    //console.log(frase)
    setFrase(res[0]);
  };

  return (
    <Contenedor>
      {/* COMPONENTE FRASE */}
      <Frase sasa={frase} />
      {/*FIN COMPONENTE FRASE */}
      <Boton onClick={consultarAPI}>Ver más frases</Boton>
    </Contenedor>
  );
}

export default App;
